using System.Drawing;
using System.IO;

namespace Motivator
{
    public class Loader
    {
        public Bitmap GetImage(string path)
        {
            using FileStream stream = File.OpenRead(path);
            var image = new Bitmap(stream);
            return image;
        }
    }
}