using System.Drawing;

namespace Motivator
{
    public class Creator
    {
        private Loader _loader;
        private GraphicWorker _graphicWorker;
        private Formatter _formatter;
        private Saver _saver;
        public Creator()
        {
            _loader = new Loader();
            _formatter = new Formatter();
            _saver = new Saver();
        }
        
        public string CreateMotivator(string image, string message)
        {
            var bitmapImage = _loader.GetImage(image);
            var resized = _formatter.ChangeSize(bitmapImage);
            ModifyImage(message, resized, bitmapImage);
            return _saver.Save(resized, image);
        }

        private void ModifyImage(string txt, Bitmap resized, Bitmap image)
        {
            _graphicWorker = new GraphicWorker(resized, image);
            _graphicWorker.ModifyImage(txt);
        }
    }
}