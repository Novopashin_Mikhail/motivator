﻿using System;

namespace Motivator
{
    class Program
    {
        static void Main(string[] args)
        {
            //var imagePath = @"C:\ProgrammingEducation\OTUS\csharp\Motivator\Motivator\Motivator\image.png";
            var imagePath = @"C:\ProgrammingEducation\OTUS\csharp\Motivator\Motivator\Motivator\image2.png";
            //var imagePath = @"C:\ProgrammingEducation\OTUS\csharp\Motivator\Motivator\Motivator\image3.png";
            var text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
            
            var creator = new Creator();
            var motivatorFileName = creator.CreateMotivator(imagePath, text);
            Console.WriteLine(motivatorFileName);
        }
    }
}