using System.Drawing;
using System.Drawing.Imaging;

namespace Motivator
{
    public class Saver
    {
        public string Save(Bitmap completeImage, string imagePathOut)
        {
            var path = imagePathOut.Split('.');
            var part1 = $"{path[0]}-complete";
            var pathOut = $"{part1}.{path[1]}";
            completeImage.Save(pathOut, ImageFormat.Png);
            return pathOut;
        }
    }
}