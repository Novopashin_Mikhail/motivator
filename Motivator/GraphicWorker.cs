using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Motivator
{
    public class GraphicWorker
    {
        private Bitmap _resized;
        private Bitmap _image;

        private int borderWidth = 62;
        private int newInitialImageWidth = 900;
        private int newInitialImageHeight = 700;

        private int botomRectangleHeight = 200;

        private int penWithForMainBorder = 124;
        private int penWithForSecondBorder = 5;

        private int startCoordinate = 0;
        
        public GraphicWorker(Bitmap resized, Bitmap image)
        {
            _resized = resized;
            _image = image;
        }
        
        public void ModifyImage(string text)
        {
            using var graphics = Graphics.FromImage(_resized);
            graphics.CompositingQuality = CompositingQuality.HighSpeed;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            ChangeSizeInitialImage(graphics);
            CreateMainBorder(graphics);
            FillBottomRectangle(graphics);
            CreateSecondeBorder(graphics);
            WriteMotivationText(graphics, text);
            graphics.Flush();
        }

        private void ChangeSizeInitialImage(Graphics graphics)
        {
            graphics.DrawImage(_image, borderWidth,borderWidth,newInitialImageWidth,newInitialImageHeight);
        }

        private void CreateMainBorder(Graphics graphics)
        {
            graphics.DrawRectangle(new Pen(Brushes.Black, penWithForMainBorder), new Rectangle(startCoordinate,startCoordinate,_resized.Width,_resized.Height));
        }

        private void CreateSecondeBorder(Graphics graphics)
        {
            graphics.DrawRectangle(new Pen(Brushes.Yellow, penWithForSecondBorder), new Rectangle(borderWidth,borderWidth,newInitialImageWidth,newInitialImageHeight));
        }

        private void FillBottomRectangle(Graphics graphics)
        {
            graphics.FillRectangle(new SolidBrush(Color.Black), new Rectangle(borderWidth, borderWidth+newInitialImageHeight, newInitialImageWidth, botomRectangleHeight));
        }

        private void WriteMotivationText(Graphics graphics, string text)
        {
            var rectangleForText = new RectangleF(borderWidth+30, newInitialImageHeight+borderWidth+30, newInitialImageWidth-30, newInitialImageHeight-30);
            graphics.DrawString(text, font: new Font("Tahoma", 48), brush: Brushes.White,layoutRectangle: rectangleForText);
        }
    }
}