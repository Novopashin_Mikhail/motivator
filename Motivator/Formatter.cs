using System.Drawing;

namespace Motivator
{
    public class Formatter
    {
        readonly int width = 1024;
        readonly int height = 1024;

        public Bitmap ChangeSize(Bitmap image)
        {
            return new Bitmap(width, height);
        }
    }
}